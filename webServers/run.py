#!/usr/bin/env python3
import boto3
import asg
import image
import time
import sys

if len(sys.argv) == 3 and type(sys.argv[1]) is str and type(sys.argv[2]) is str:
    nelb = sys.argv[2]+"ELB"
    nlc = sys.argv[2]+"LC"
    nasg = sys.argv[2]+"ASG"
    asg.create(image.getImageId(sys.argv[1]), nlc, nasg, nelb)
else:
    print("Syntax error: "+sys.argv[0]+"<imagename> <label> (arguments must be strings)")
