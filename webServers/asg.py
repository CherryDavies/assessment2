#!/usr/bin/env python3
import boto3

def create(imid, lcname, asgname, lbname):
    asgclient = boto3.client('autoscaling','eu-west-2')
    lc=asgclient.create_launch_configuration(
        LaunchConfigurationName=lcname,
        ImageId= imid,
        InstanceType='t2.micro',
        SecurityGroups = ['sg-04df915d2d79b0305'],
        AssociatePublicIpAddress=True,
    )
    print("Create launch configuration "+lcname+" from image "+imid)
    asg=asgclient.create_auto_scaling_group(
        AutoScalingGroupName=asgname,
        LaunchConfigurationName=lcname,
        MinSize=1,
        MaxSize=3,
        DesiredCapacity=1,
        LoadBalancerNames=[lbname],
        AvailabilityZones=['eu-west-2a','eu-west-2b'],
        VPCZoneIdentifier='subnet-01621ca460c3fdf66,subnet-0e0476aae93d8d2c0'
    )
    print("Create auto-scaling group "+asgname)
    attach=asgclient.attach_load_balancers(
        AutoScalingGroupName=asgname,
        LoadBalancerNames=[lbname]
    )
    print("Attached load balancer "+lbname+" to "+asgname)
