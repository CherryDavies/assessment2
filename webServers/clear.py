#!/usr/bin/env python3
import boto3

client = boto3.client('autoscaling','eu-west-2')
response = client.describe_auto_scaling_groups()
for asg in response['AutoScalingGroups']:
    for elb in asg['LoadBalancerNames']:
        client.detach_load_balancers(
            AutoScalingGroupName=asg['AutoScalingGroupName'],
            LoadBalancerNames=[
                elb,
            ]
        )
        print("Detached "+asg['AutoScalingGroupName']+" from "+elb)
    client.delete_auto_scaling_group(
        AutoScalingGroupName=asg['AutoScalingGroupName'],
        ForceDelete=True
    )
    print("Deleted ASG: "+asg['AutoScalingGroupName'])

response = client.describe_launch_configurations()
for lc in response['LaunchConfigurations']:
    response = client.delete_launch_configuration(
        LaunchConfigurationName=lc['LaunchConfigurationName']
    )
    print("Deleted LC: "+lc['LaunchConfigurationName'])
