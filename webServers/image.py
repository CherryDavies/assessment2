#!/usr/bin/env python3
import boto3

def getImageId(imagename):
    ec2client = boto3.client('ec2','eu-west-2')
    response = ec2client.describe_images(
        Filters=[
            {
                'Name': 'name',
                'Values': [
                    imagename,
                ]
            },
        ],
    )
    return response['Images'][0]['ImageId']
