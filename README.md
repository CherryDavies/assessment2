# AWS automation (Cloud Assessment2)

This project shows the build of a set of virtual machines in the cloud that provide web and database services for a PHP app server.

Client is happy to be given one (production) environment as proof-of-concept before building several environments for dev, test and QA. The servers, databases and jobs below have been provisioned to function in an automated manner to satisfy our client. 

* Build a jenkins server
* Build a PHP app server (Apache)
* Build a Database Server
* Build an nginx web server
* Build jobs on jenkins

To access our jenkins server: (http://jenkins.dreamteam.academy.grads.al-labs.co.uk:8080/)

## Generating the Jenkins server(Running the tests):

The Assessment2 directory includes code to generate a fully provisioned Jenkins server with all the appropriate plugins. Small adjustments such as the region, subnets, SecurityGroupIds would be required in order to satisfy the clients application. Below is a simple command in order to run the server:

From the jenkins directory run the Runinstance script:
```
Python3 Runinstance
```
in return you should obtain a public IP Address for the Jenkins server which you can access from your web browser on port:8080.
*Once on the Jenkins server you can check for the relevant plugins being installed ensuring the correct provisioning of the server has occured via the jenkins.log.

### Essential builds required for operating on Jenkins:
* Maven - Dependency Management
* Java
* Python3
* git
