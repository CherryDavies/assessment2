#!/usr/bin/env python3
import boto3
import json

ARPDstring = '{ "Version": "2012-10-17", "Statement": [ { "Effect": "Allow", "Principal": { "Service": "ec2.amazonaws.com" }, "Action": "sts:AssumeRole" } ] }'

def addFullAccessRole(name, region):
    iam = boto3.client('iam', region)
    ARPDstring = '{ "Version": "2012-10-17", "Statement": [ { "Effect": "Allow", "Principal": { "Service": "ec2.amazonaws.com" }, "Action": "sts:AssumeRole" } ] }'
    role = iam.create_role(
        RoleName=name,
        AssumeRolePolicyDocument=ARPDstring,
        Description='Role giving full EC2 access',
        Tags=[
            {
                'Key': 'Team',
                'Value': 'Dream Team'
            },
        ]
    )
    rolepolicy = iam.attach_role_policy(
        RoleName=name,
        PolicyArn='arn:aws:iam::aws:policy/AmazonEC2FullAccess'
    )
    ipname = name+"InstanceProfile"
    instanceProf = iam.create_instance_profile(
        InstanceProfileName=ipname
    )
    response = iam.add_role_to_instance_profile(
        InstanceProfileName=ipname,
        RoleName=name
    )

def addReadAccesRole(name, region):
    iam = boto3.client('iam', region)
    ARPDstring = '{ "Version": "2012-10-17", "Statement": [ { "Effect": "Allow", "Principal": { "Service": "ec2.amazonaws.com" }, "Action": "sts:AssumeRole" } ] }'
    response = iam.create_role(
        RoleName=name,
        AssumeRolePolicyDocument=ARPDstring,
        Description='Role giving read EC2 access',
        Tags=[
            {
                'Key': 'Team',
                'Value': 'Dream Team'
            },
        ]
    )
    rolepolicy = iam.attach_role_policy(
        RoleName=name,
        PolicyArn='arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess'
    )
