#!/usr/bin/env python3
import boto3
import json

def read():
    fh = open('setupFile.txt','r')
    string = fh.read()
    dict = json.loads(string)
    fh.close()
    return dict

def update(inDict):
    fr = open('setupFile.txt','r')
    string = fr.read()
    dict = json.loads(string)
    fr.close()
    fw = open('updateFile.txt','w')
    dict.update(inDict)
    fw.write(json.dumps(dict))
    return dict

def readUpdate():
    fh = open('updateFile.txt','r')
    string = fh.read()
    dict = json.loads(string)
    fh.close()
    return dict
