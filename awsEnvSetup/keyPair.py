#!/usr/bin/env python3
import boto3
import sys

def create(resource, name, filename):
    response = resource.create_key_pair(
        KeyName=name
    )
    print("Created key pair: "+response.name+"\n\tID: "+response.key_pair_id)
    keyfile = open(filename, 'w')
    print("Writing key to "+filename)
    keyfile.write(response.key_material)
    return response.name

def download(resource, name, filename):
    kp = resource.KeyPair(name)
    print("Accessed key pair: "+kp.name+"\n\tID: "+kp.key_pair_id)
    keyfile = open(filename, 'w')
    print("Writing key to "+filename)
    keyfile.write(kp.key_material)
