#!/usr/bin/env python3
import boto3

def create(elbclient, lbname, ps1, ps2, sg):
    elb=elbclient.create_load_balancer(
        LoadBalancerName=lbname,
        Listeners=[
        {
            'Protocol': 'HTTP',
            'LoadBalancerPort': 80,
            'InstanceProtocol': 'HTTP',
            'InstancePort': 80
        }
        ],
        #Subnets=['subnet-01621ca460c3fdf66','subnet-0e0476aae93d8d2c0'],
        Subnets=[ps1,ps2],
        #SecurityGroups = ['sg-04df915d2d79b0305'],
        SecurityGroups = [sg],
        Tags=[
            {
                'Key': 'Name',
                'Value': lbname
            }
        ]
    )
    print("Created elb "+lbname)
