#!/usr/bin/env python3
import boto3
import time

def create(resource, name, cidr):
    vpc = resource.create_vpc(CidrBlock=cidr)
    tag = vpc.create_tags(Tags=[{'Key': 'Name','Value': name}])
    print("Created VPC with name: "+name)
    return vpc

def createInternetGateway(resource, vpc, name):
    igw = resource.create_internet_gateway()
    vpc.attach_internet_gateway(InternetGatewayId=igw.internet_gateway_id)
    tag = igw.create_tags(Tags=[{'Key': 'Name','Value': name}])
    print("Created internet gateway with name: "+name+"\n\tand ID:"+igw.internet_gateway_id)
    return igw

def makePubSubnet(vpc, igw, cidr, name, az):
    pubSub = vpc.create_subnet(CidrBlock=cidr, AvailabilityZone=az)
    tag = pubSub.create_tags(Tags=[{'Key': 'Name','Value': name}])
    pubSub.meta.client.modify_subnet_attribute(SubnetId=pubSub.id, MapPublicIpOnLaunch={"Value": True})
    pubRouteTable = vpc.create_route_table()
    tag = pubRouteTable.create_tags(Tags=[{'Key': 'Name','Value': name}])
    igwRoute = pubRouteTable.create_route(
        DestinationCidrBlock='0.0.0.0/0',
        GatewayId=igw.internet_gateway_id
    )
    pubAssoc = pubRouteTable.associate_with_subnet(SubnetId=pubSub.subnet_id)
    print("Created public subnet with name: "+name+"\n\tand ID:"+pubSub.subnet_id)
    return pubSub

def createNatGateway(client,pubSub):
    dict = client.allocate_address(
        Domain='vpc',
    )
    natgat = client.create_nat_gateway(
        AllocationId=dict['AllocationId'],
        SubnetId=pubSub.subnet_id
    )
    id = natgat['NatGateway']['NatGatewayId']
    describe = client.describe_nat_gateways(NatGatewayIds=[id])
    print("Creating NAT gateway")
    while (describe['NatGateways'][0]['State'] != 'available'):
        time.sleep(5)
        print(".")
        describe = client.describe_nat_gateways(NatGatewayIds=[id])
    print("Created. ID: "+id+"\n\tPublic IP: "+dict['PublicIp'])
    return [id, dict['PublicIp']]

def makePrivSubnet(vpc, cidr, natgwID, name, az):
    privSub = vpc.create_subnet(CidrBlock=cidr, AvailabilityZone=az)
    tag = privSub.create_tags(Tags=[{'Key': 'Name','Value': name}])
    privRouteTable = vpc.create_route_table()
    tag = privRouteTable.create_tags(Tags=[{'Key': 'Name','Value': name}])
    ngwRoute = privRouteTable.create_route(
        DestinationCidrBlock='0.0.0.0/0',
        NatGatewayId=natgwID
    )
    privAssoc = privRouteTable.associate_with_subnet(SubnetId=privSub.subnet_id)
    print("Created private subnet with name: "+name+"\n\tand ID:"+privSub.subnet_id)
    return privSub
