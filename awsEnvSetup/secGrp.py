#!/usr/bin/env python3
import boto3

def jenkinsIpList():
    ips = []
    for port in [22,8080]:
        ips.append(
        {'IpProtocol': 'tcp',
         'FromPort': port,
         'ToPort': port,
         'IpRanges': [{'CidrIp': '86.23.63.142/32'}, {'CidrIp': '86.4.90.40/32'}, {'CidrIp': '86.132.201.30/32'}]
        })
    return ips

def petClinicIpList():
    ips = []
    ips.append(
    {'IpProtocol': 'tcp',
     'FromPort': 8080,
     'ToPort': 8080,
     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
    })
    ips.append(
    {'IpProtocol': 'tcp',
     'FromPort': 22,
     'ToPort': 22,
     'IpRanges': [{'CidrIp': '86.23.63.142/32'}, {'CidrIp': '86.4.90.40/32'}, {'CidrIp': '86.132.201.30/32'}]
    })
    ips.append(
    {'IpProtocol': '-1',
     'IpRanges': [{'CidrIp': '172.30.0.0/16'}]
    })
    return ips

def webServerIpList():
    ips = []
    for port in [80,443,22]:
        if port == 80 or port == 443:
            ips.append(
            {'IpProtocol': 'tcp',
             'FromPort': port,
             'ToPort': port,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
            })
        else:
            ips.append(
            {'IpProtocol': 'tcp',
             'FromPort': port,
             'ToPort': port,
             'IpRanges': [{'CidrIp': '86.23.63.142/32'}, {'CidrIp': '86.4.90.40/32'}, {'CidrIp': '86.132.201.30/32'}]
            })
    ips.append(
    {'IpProtocol': '-1',
     'IpRanges': [{'CidrIp': '172.30.0.0/16'}]
    })
    return ips

def dbIpList():
    ips = []
    ips.append(
    {'IpProtocol': 'tcp',
     'FromPort': 3306,
     'ToPort': 3306,
     'IpRanges': [{'CidrIp': '172.30.0.0/16'}, {'CidrIp': '86.23.63.142/32'}, {'CidrIp': '86.4.90.40/32'}, {'CidrIp': '86.132.201.30/32'}]
    })
    ips.append(
    {'IpProtocol': 'tcp',
     'FromPort': 22,
     'ToPort': 22,
     'IpRanges': [{'CidrIp': '86.23.26.210/32'}, {'CidrIp': '86.4.90.40/32'}, {'CidrIp': '86.132.201.30/32'}]
    })
    ips.append(
    {'IpProtocol': '-1',
     'IpRanges': [{'CidrIp': '172.30.0.0/16'}]
    })
    return ips

def create(resource, name, ips, vpcid, description='Made using Python'):
    response = resource.create_security_group(Description = description, GroupName = name, VpcId=vpcid)
    groupID = response.id
    print("Security group created with ID: "+str(groupID))
    data = response.authorize_ingress(GroupId=groupID, IpPermissions=ips)
    return groupID
