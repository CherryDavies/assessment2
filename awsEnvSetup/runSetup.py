#!/usr/bin/env python3
import boto3
import secGrp
import json
import setup
import keyPair
import sys
import iam
import vpc
import elb

def secGrps(resource, vpcid, name):
    nm = name+'dbSecGrp'
    secGrp.create(resource, nm, secGrp.dbIpList(), vpcid, 'Sec grp for db server for Dream Team')
    nm = name+'jenkinsSecGrp'
    secGrp.create(resource, nm, secGrp.jenkinsIpList(), vpcid, 'Sec grp for Jenkins server for Dream Team')
    nm = name+'pcSecGrp'
    secGrp.create(resource, nm, secGrp.petClinicIpList(), vpcid, 'Sec grp for PetClinic servers for Dream Team')
    nm = name+'wsSecGrp'
    ws = secGrp.create(resource, nm, secGrp.webServerIpList(), vpcid, 'Sec grp for web servers for Dream Team')
    return ws

def makeKeys(resource, bucketname):
    keyPair.create(resource,"jenKey","/Users/thomasmoran/.ssh/alga_aws_london_jenkins.pem")
    keyPair.create(resource,"phpKey","/Users/thomasmoran/.ssh/alga_aws_london_php.pem")
    keyPair.create(resource,"wsKey","/Users/thomasmoran/.ssh/alga_aws_london_webs.pem")
    s3 = boto3.resource('s3')
    s3.Bucket(bucketname).upload_file('/Users/thomasmoran/.ssh/alga_aws_london_jenkins.pem', 'alga_aws_london_jenkins.pem')
    s3.Bucket(bucketname).upload_file('/Users/thomasmoran/.ssh/alga_aws_london_php.pem', 'alga_aws_london_php.pem')
    s3.Bucket(bucketname).upload_file('/Users/thomasmoran/.ssh/alga_aws_london_webs.pem', 'alga_aws_london_webs.pem')

def addIamRoles(region):
    iam.addFullAccessRole('jenkinsRole',region)

def setVPC(resource,region,name,cidrStart):
    vpcname = name+'VPC'
    vpcCidr = cidrStart+'0.0/16'
    VPC = vpc.create(resource, vpcname, vpcCidr)
    igname = name+"IG"
    IGW = vpc.createInternetGateway(resource, VPC, igname)
    eclient = boto3.client('ec2',region)
    az1 = region+"a"
    psname = name+'PubNet1'
    psCidr = cidrStart+'0.0/18'
    pubSub1 = vpc.makePubSubnet(VPC, IGW, psCidr, psname, az1)
    NatGat1 = vpc.createNatGateway(eclient,pubSub1)
    psname = name+'privNet1'
    psCidr = cidrStart+'64.0/18'
    privSub1 = vpc.makePrivSubnet(VPC, psCidr, NatGat1[0], psname, az1)
    az2 = region+"b"
    psname = name+'PubNet2'
    psCidr = cidrStart+'128.0/18'
    pubSub2 = vpc.makePubSubnet(VPC, IGW, psCidr, psname, az2)
    NatGat2 = vpc.createNatGateway(eclient,pubSub1)
    psname = name+'privNet2'
    psCidr = cidrStart+'192.0/18'
    privSub2 = vpc.makePrivSubnet(VPC, psCidr, NatGat1[0], psname, az2)

    vpcDict = {"Name": name, "vpcId": VPC.vpc_id, "pubSub1Id": pubSub1.subnet_id, "pubSub2Id": pubSub2.subnet_id, "privSub1Id": privSub1.subnet_id, "privSub2Id": privSub2.subnet_id}
    setup.update(vpcDict)
    return vpcDict

def loadbalancer(region, ps1, ps2, webSG):
    client = boto3.client('elb', region)
    elb.create(client, "phpELB", ps1, ps2, webSG)
    elb.create(client, "wsELB", ps1, ps2, webSG)

if len(sys.argv) == 2:
    dict = setup.read()
    ec2 = boto3.resource('ec2',dict['region'])
    if sys.argv[1] == 'fullsetup':
        vpcDict = setVPC(ec2,dict['region'],'dt','172.30.')
        webSG = secGrps(ec2, vpcDict['vpcID'], 'dt_')
        makeKeys(ec2, "thekeys2dream")
        addIamRoles(dict['region'])
        loadbalancer(dict['region'], vpcDict['pubSub1Id'], vpcDict['pubSub2Id'], webSG)
    elif sys.argv[1] == 'secgrp':
        vpcid = 'vpc-02f7c9051997d8da2'
        secGrps(ec2, vpcid, 'dt_')
    elif sys.argv[1] == 'makekeys':
        makeKeys(ec2, "thekeys2dream")
    elif sys.argv[1] == 'iamroles':
        addIamRoles(dict['region'])
    elif sys.argv[1] == 'setvpc':
        setVPC(ec2,dict['region'],'dt','172.30.')
    elif sys.argv[1] == 'elb':
        loadbalancer(dict['region'], 'subnet-01621ca460c3fdf66', 'subnet-0e0476aae93d8d2c0', 'sg-04df915d2d79b0305')
    else:
        print("Need to add option: "+sys.argv[0]+" <fullsetup/secgrp/makekeys/dlkeys/iamroles>")
        sys.exit(1)
else:
    print("Need to add option: "+sys.argv[0]+" <fullsetup/secgrp/makekeys/dlkeys/iamroles>")
    sys.exit(1)
