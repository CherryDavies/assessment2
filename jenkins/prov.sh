#!/bin/bash

# yum -y update

# This version installs Jenkins
# Adds the admin user with password secret from XML template
# Addes a single job through the API

# Check if Jenkins is already running
if rpm -qa | grep jenkins >/dev/null 2>&1
then
	service jenkins stop >/dev/null 2>&1
	>/var/log/jenkins/jenkins.log
fi

sudo yum -y install java wget git python3
if [[ ! -d /etc/yum.repos.d/jenkins.repo ]]
then
	sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat/jenkins.repo
	sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key
	sudo yum -y install jenkins
fi

# Extract a previous base configuration
sudo	yum -y install jenkins
sudo wget https://github.com/stevshil/vagrant/blob/master/jenkins/files/jenkins-2.235.tgz?raw=true -O /tmp/jenkins-2.235.tgz
cd /var/lib
sudo tar xvf /tmp/jenkins-2.235.tgz

# Start jenkins to get the directories
echo "Starting Jenkins for the first time"
sudo service jenkins start
echo "Waiting for Jenkins"
until grep "Jenkins is fully up and running" /var/log/jenkins/jenkins.log >/dev/null 2>&1
do
	echo -n "."
	sleep 15
done
echo ""

# Install plugins
sudo curl -L https://raw.githubusercontent.com/hgomez/devops-incubator/master/forge-tricks/batch-install-jenkins-plugins.sh -o /tmp/batch-install-jenkins-plugins.sh
sudo chmod 755 /tmp/batch-install-jenkins-plugins.sh
cat >/tmp/pluginlist <<_end
cloudbees-folder
configuration-as-code
antisamy-markup-formatter
build-timeout
credentials-binding
timestamper
ws-cleanup
jaxb
bouncycastle-api
jdk-tool
command-launcher
workflow-aggregator
github-branch-source
git
github
git-parameter
pipeline-stage-tags-metadata
pipeline-model-declarative-agent
pipeline-github-lib
pipeline-stage-view
pipeline-build-step
pipeline-milestone-step
pipeline-model-definition
pipeline
maven
ssh-slaves
matrix-auth
pam-auth
ldap
email-ext
mailer
_end
sudo /tmp/batch-install-jenkins-plugins.sh --plugins /tmp/pluginlist --plugindir /var/lib/jenkins/plugins

# Restart to initialise plugins
sudo service jenkins stop
# Empty log to check for running
>/var/log/jenkins/jenkins.log
sudo service jenkins start
echo "Waiting for Jenkins"
until grep "Jenkins is fully up and running" /var/log/jenkins/jenkins.log >/dev/null 2>&1
do
	echo -n "."
	sleep 15
done
echo ""

#echo "Creating first job"
#cd /vagrant/files
#./createJob hello_world
#systemctl restart jenkins.service
# service jenkins start
